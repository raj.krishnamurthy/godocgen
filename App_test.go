package godocgen

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	// switches
	processInputRecursively = "processInputRecursively"
	useBadInputPath         = "useBadInputPath"
	useEmptyInputPath       = "useEmptyInputPath"
	useFileInputPath        = "useFileInputPath"
	useNonGoInputPath       = "useNonGoInputPath"
	useProperInputPath      = "useProperInputPath"

	// values
	badInputPath    = "./internal/testpkg/internal/badInputPathAtAll"
	emptyInputPath  = ""
	fileInputPath   = "./internal/testpkg/internal/fileTypePkg"
	nonGoInputPath  = "./internal/testpkg/internal/improper"
	properInputPath = "./internal/testpkg"
	recursiveInput  = "..."
)

const (
	// switches
	useEmptyOutputPath    = "useEmptyOutputPath"
	useFileOutputPath     = "useFileOutputPath"
	useProperOutputPath   = "useProperOutputPath"
	useTerminalOutputPath = "useTerminalOutputPath"

	// values
	emptyOutputPath    = ""
	fileOutputPath     = "./test/badOutput"
	properOutputPath   = "./test/outputs/txt"
	terminalOutputPath = Terminal
)

const (
	// switches
	useBadFilename      = "useBadFilename"
	useMarkdownFilename = "useMarkdownFilename"
	useNoFilename       = "useNoFilename"
	useProperFilename   = "useProperFilename"

	// values
	badFilename    = "_index"
	mdFilename     = "_index.md"
	noFilename     = ""
	properFilename = "_index.txt"
)

const (
	// switches
	useBadTemplatePath       = "useBadTemplatePath"
	useDirectoryTemplatePath = "useDirectoryTemplatePath"
	useMarkdownTemplatePath  = "useMarkdownTemplatePath"
	useNoTemplatePath        = "useNoTemplatePath"
	useProperTemplatePath    = "useProperTemplatePath"

	// values
	badTemplatePath       = "./test/templates/atbaetnbrstjaaetnanar"
	directoryTemplatePath = "./test/templates/directory"
	mdTemplatePath        = "./.godocgen/templates/md"
	noTemplatePath        = ""
	properTemplatePath    = "./.godocgen/templates/txt"
)

const (
	// switches
	useAppMode     = "useAppMode"
	useLibraryMode = "useLibraryMode"
	useMockMode    = "useMockMode"
	useUnknownMode = "useUnknownMode"

	// values
	appMode     = AppMode
	libraryMode = LibraryMode
	mockMode    = MockMode
	unknownMode = 125
)

const (
	// switches
	useDefaultWidth = "useDefaultWidth"
	useZeroWidth    = "useZeroWidth"

	// values
	defaultWidth = 70
	zeroWidth    = 0
)

const (
	// switches
	simulateBadABSOutputPath    = "simulateBadAbsOutputPath"
	simulateBadStatOutputPath   = "simulateBadStatOutputPath"
	simulateBadABSInputPath     = "simulateBadABSInputPath"
	simulateFailedFileCreation  = "simulateFailedFaileCreation"
	simulateFailedCanvaCreation = "simulateFailedCanvaCreation"
	simulateGeneratorError      = "simulateGeneratorError"
)

const (
	testRun = "testRun"
)

type testScenario thelper.Scenario

func (s *testScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testScenario) log(th *thelper.THelper, data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testScenario) PrepareTestEnvironment() {
	// clean up ./test/outputs
	os.RemoveAll(filepath.Dir(testTerminalOutputPath))
	_ = os.MkdirAll(filepath.Dir(testTerminalOutputPath), os.ModePerm)

	// clean up ./test/templates/directory
	os.RemoveAll(directoryTemplatePath)
	_ = os.MkdirAll(directoryTemplatePath, os.ModePerm)

	// re-create ./test/
	os.RemoveAll(fileOutputPath)

	file, err := os.Create(fileOutputPath)
	if err == nil {
		b := bufio.NewWriter(file)
		fmt.Fprintf(file, "this is a file type output")
		b.Flush()
		file.Close()
	}
}

func (s *testScenario) Configure(x *App) {
	x.testConfig[testMode] = true
	s.configureAppMode(x)
	s.configureInputPath(x)
	s.configureOutputPath(x)
	s.configureFilename(x)
	s.configureTemplatePath(x)
	s.configureWidth(x)
	s.configureTestSimulation(x)
}

func (s *testScenario) configureAppMode(x *App) {
	switch {
	case s.Switches[useAppMode]:
		x.WorkMode = appMode
	case s.Switches[useLibraryMode]:
		x.WorkMode = libraryMode
	case s.Switches[useMockMode]:
		x.WorkMode = mockMode
	case s.Switches[useUnknownMode]:
		x.WorkMode = unknownMode
	default:
	}
}

func (s *testScenario) configureInputPath(x *App) {
	switch {
	case s.Switches[useProperInputPath]:
		x.InputPath = properInputPath
	case s.Switches[useNonGoInputPath]:
		x.InputPath = nonGoInputPath
	case s.Switches[useEmptyInputPath]:
		x.InputPath = emptyInputPath
	case s.Switches[useBadInputPath]:
		x.InputPath = badInputPath
	case s.Switches[useFileInputPath]:
		x.InputPath = fileInputPath
	default:
	}

	if s.Switches[processInputRecursively] {
		if x.InputPath == emptyInputPath {
			x.InputPath = recursiveInput
		} else {
			x.InputPath = filepath.Join(x.InputPath, recursiveInput)
		}
	}
}

func (s *testScenario) configureOutputPath(x *App) {
	switch {
	case s.Switches[useProperOutputPath]:
		x.OutputPath = properOutputPath
	case s.Switches[useTerminalOutputPath]:
		x.OutputPath = terminalOutputPath
	case s.Switches[useEmptyOutputPath]:
		x.OutputPath = emptyOutputPath
	case s.Switches[useFileOutputPath]:
		x.OutputPath = fileOutputPath
	default:
	}
}

func (s *testScenario) configureTemplatePath(x *App) {
	switch {
	case s.Switches[useProperTemplatePath]:
		x.TemplatePath = properTemplatePath
	case s.Switches[useNoTemplatePath]:
		x.TemplatePath = noTemplatePath
	case s.Switches[useDirectoryTemplatePath]:
		x.TemplatePath = directoryTemplatePath
	case s.Switches[useBadTemplatePath]:
		x.TemplatePath = badTemplatePath
	case s.Switches[useMarkdownTemplatePath]:
		x.TemplatePath = mdTemplatePath
	default:
	}
}

func (s *testScenario) configureFilename(x *App) {
	switch {
	case s.Switches[useProperFilename]:
		x.Filename = properFilename
	case s.Switches[useNoFilename]:
		x.Filename = noFilename
	case s.Switches[useBadFilename]:
		x.Filename = badFilename
	case s.Switches[useMarkdownTemplatePath]:
		x.Filename = mdFilename
	default:
	}
}

func (s *testScenario) configureWidth(x *App) {
	switch {
	case s.Switches[useDefaultWidth]:
		x.Width = defaultWidth
	case s.Switches[useZeroWidth]:
		x.Width = zeroWidth
	default:
	}
}

func (s *testScenario) configureTestSimulation(x *App) {
	if s.Switches[simulateGeneratorError] {
		x.testConfig[testSimulateGeneratorError] = true
	}

	if s.Switches[simulateBadABSOutputPath] {
		x.testConfig[testSimulateFailedABSOutput] = true
	}

	if s.Switches[simulateBadStatOutputPath] {
		x.testConfig[testSimulateBadStatOutput] = true
	}

	if s.Switches[simulateBadABSInputPath] {
		x.testConfig[testSimulateFailedABSFilepath] = true
	}

	if s.Switches[simulateFailedCanvaCreation] {
		x.testConfig[testSimulateFailedCanvaCreation] = true
	}

	if s.Switches[simulateFailedFileCreation] {
		x.testConfig[testSimulateFailedFileCreation] = true
	}
}

func (s *testScenario) scanForInputPathError(th *thelper.THelper, x *App) {
	if s.Switches[useMockMode] || s.Switches[useUnknownMode] {
		return
	}

	// Negative cases. Scan for true negative...
	err := ""

	switch {
	case s.Switches[useEmptyInputPath]:
		if !s.Switches[processInputRecursively] {
			err = ErrorMissingInputPath
		}
	case s.Switches[simulateBadABSInputPath]:
		err = ErrorBadInputPath
	case s.Switches[useFileInputPath]:
		err = ErrorInputPathNotDirectory
	case s.Switches[useNonGoInputPath]:
		err = ErrorInputPathWithoutGo
	}

	if err != "" {
		s.checkExitCode(th, x.ExitCode, ErrorCodeCommon, false)
		s.checkForError(th, x.Log, err, false)

		return
	}

	// Positive cases. Now scan for false negative...
	s.checkForError(th, x.Log, ErrorMissingInputPath, true)
	s.checkForError(th, x.Log, ErrorBadInputPath, true)
	s.checkForError(th, x.Log, ErrorInputPathNotDirectory, true)

	if !s.Switches[processInputRecursively] {
		s.checkForError(th, x.Log, ErrorInputPathWithoutGo, true)
	}
}

func (s *testScenario) scanForOutputPathError(th *thelper.THelper, x *App) {
	if s.Switches[useMockMode] || s.Switches[useUnknownMode] {
		return
	}

	// Negative cases. Scan for true negative...
	err := ""

	switch {
	case s.Switches[useEmptyOutputPath]:
		err = ErrorMissingOutputPath
	case s.Switches[useFileOutputPath]:
		err = ErrorOutputPathIsAFile
	case s.Switches[simulateBadABSOutputPath]:
		err = ErrorBadOutputPath
	case s.Switches[simulateBadStatOutputPath]:
		err = errorSimulateBadStatOutput
	}

	if err != "" {
		s.checkExitCode(th, x.ExitCode, ErrorCodeCommon, false)
		s.checkForError(th, x.Log, err, false)

		return
	}

	// Positive cases. Now scan for false negative...
	s.checkForError(th, x.Log, ErrorMissingOutputPath, true)
	s.checkForError(th, x.Log, ErrorOutputPathIsAFile, true)
	s.checkForError(th, x.Log, ErrorBadOutputPath, true)
	s.checkForError(th, x.Log, errorSimulateBadStatOutput, true)
}

func (s *testScenario) scanForTemplatePathError(th *thelper.THelper, x *App) {
	if s.Switches[useMockMode] || s.Switches[useUnknownMode] {
		return
	}

	// Negative cases. Scan for true negative...
	err := ""

	switch {
	case s.Switches[useBadTemplatePath]:
		err = ErrorBadTemplatePath
	case s.Switches[useDirectoryTemplatePath]:
		err = ErrorTemplatePathIsADir
	case s.Switches[useProperOutputPath] && s.Switches[useNoTemplatePath]:
		err = ErrorMissingTemplatePath
	}

	if err != "" {
		s.checkForError(th, x.Log, err, false)
		return
	}

	// Positive cases. Now scan for false negative...
	s.checkForError(th, x.Log, ErrorBadTemplatePath, true)
	s.checkForError(th, x.Log, ErrorTemplatePathIsADir, true)
}

func (s *testScenario) scanForOutput(th *thelper.THelper, x *App) {
	hasError := s.checkLog(x.Log, ErrorTag)

	if hasError && len(x.Targets) > 0 {
		th.Errorf("got output despite having error.")
		return
	}

	for _, datum := range x.Targets {
		switch {
		case s.Switches[useTerminalOutputPath]:
			s.renderedFileExists(th, testTerminalOutputPath)
		case s.Switches[useBadFilename],
			s.Switches[useLibraryMode],
			s.Switches[useMockMode],
			s.Switches[simulateBadABSOutputPath],
			s.Switches[simulateFailedCanvaCreation],
			s.Switches[useProperOutputPath] &&
				s.Switches[useNoTemplatePath]:
			s.renderedFileNotExist(th, datum.Filepath)
		default:
			s.renderedFileExists(th, datum.Filepath)
		}
	}
}

func (s *testScenario) renderedFileNotExist(th *thelper.THelper, path string) {
	info, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return
	}

	th.Errorf("output file/directory exists = |%v|", path)

	mode := info.Mode()

	switch {
	case mode.IsRegular():
	default:
		th.Errorf("output file is not a regular file = |%v|", path)
	}
}

func (s *testScenario) renderedFileExists(th *thelper.THelper, path string) {
	info, err := os.Stat(path)
	if err != nil {
		th.Errorf("output file has error = |%v|", err)
		return
	}

	mode := info.Mode()

	switch {
	case mode.IsRegular():
	default:
		th.Errorf("output file is not a regular file = |%v|", path)
	}
}

func (s *testScenario) checkLog(log []string, keyword string) bool {
	for _, line := range log {
		if strings.Contains(line, keyword) {
			return true
		}
	}

	return false
}

func (s *testScenario) checkExitCode(th *thelper.THelper,
	code int,
	expect int,
	inverseExpectation bool) {
	if code == expect {
		if inverseExpectation {
			th.Errorf("Unexpected Equal Exit Code:|%d != (Got) %d|",
				expect,
				code)
		}

		return
	}

	if !inverseExpectation {
		th.Errorf("Unexpected Exit Code:|%d != (Got) %d|",
			expect, code)
	}
}

func (s *testScenario) checkForError(th *thelper.THelper,
	log []string,
	err string,
	withoutError bool) {
	for _, line := range log {
		if strings.Contains(line, err) {
			if withoutError {
				th.Errorf("Unexpected Error:|%s|", err)
				return
			}

			return
		}
	}

	if !withoutError {
		th.Errorf("Missing Error:|%s|", err)
	}
}
