package godocgen

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// App is the master control for godocgen go package.
//
// It has private variables that require initialization. Therefore, to create
// one safely, please use `NewApp()` function.
//
// Before calling its `App.Run()` function, you need to ensure the following
// elements are fulfilled.
//
// COMPULSORY elements:
//   1. `OutputPath`   - location for storing the generated output (directory).
//   2. `InputPath`    - location for reading the go packages (directory).
//                       Append `...` in the end for recursive generations.
//   3. `WorkMode`     - how App should works such as: as a test mocking unit,
//                       package library, or as app mode.
//
// OPTIONAL elements:
//   1. `TemplatePath` - location to the template file for output rendering.
//   2. `Filename`     - output filename with extension. If this is set,
//                       `TemplatePath` must be specified.
//   3. `Width`        - to specify the maximum width of the document. Default
//                       is `80` for terminal and `70` for files.
//
// For OUTPUT, depending on `App.WorkMode`, they are different. For example,
//   1. in `godocgen.MockMode`, nothing is executed. `App` exited immediately.
//   2. in `godocgen.LibraryMode`, the output, system log, and exit code are
//      stored inside `App.Targets`, `App.Log`, and `App.Exit` respectively.
//      Rendering process is not executed in this mode, allowing one to use
//      `App.Targets` `Data` list for his/her own Go rendering.
//   3. in `godocgen.AppMode`, it will run the full-fletch executions including
//      rendering.
type App struct {
	// user input
	InputPath    string
	OutputPath   string
	Filename     string
	TemplatePath string
	WorkMode     uint
	Width        uint

	// generated output
	Targets  []*Data
	Log      []string
	ExitCode int

	// internal processing variables
	workPath   string
	exitNow    func(code int)
	recursive  bool
	testConfig map[int]bool
}

// NewApp is the function for creating the godocgen App structure object.
//
// It returns the memory pointer of the created App object. Since App structure
// contains uninitialized private elements, it is best to use this function to
// create App object.
func NewApp() *App {
	return &App{
		Targets:    []*Data{},
		Width:      70,
		WorkMode:   LibraryMode,
		testConfig: map[int]bool{},
	}
}

// Run is the function for starting the godocgen process.
//
// It executes the processing accordingly based on its WorkingMode. For AppMode,
// the rendering is inclusive and will be rendered automatically.
func (c *App) Run() {
	// configure exit function
	c.exitNow = os.Exit
	if c.testConfig[testMode] {
		c.exitNow = func(i int) { c.ExitCode = i }
	}

	// check work modes
	switch {
	case c.WorkMode == MockMode:
		return
	case c.WorkMode == LibraryMode:
		c.exitNow = func(i int) { c.ExitCode = i }
	case c.WorkMode == AppMode:
		// do nothing
	default:
		c.exit(ErrorCodeCommon, ErrorTag, ErrorUnknownWorkMode)
	}

	c.validateInputPath()
	c.validateOutputPath()
	c.validateFilename()
	c.validateTemplatePath()
	c.generateDocData()
	c.renderDocData()
}

func (c *App) validateInputPath() {
	var err error

	// check for recursive symbol
	c.recursive = false
	if filepath.Base(c.InputPath) == "..." {
		c.InputPath = filepath.Dir(c.InputPath)
		c.recursive = true
	}

	// validate input path
	if c.InputPath == "" {
		c.exit(ErrorCodeCommon, ErrorTag, ErrorMissingInputPath)
		return
	}

	// convert to absolute path
	if !filepath.IsAbs(c.InputPath) {
		c.InputPath, err = filepath.Abs(c.InputPath)
		if err != nil || c.testConfig[testSimulateFailedABSFilepath] {
			c.exit(ErrorCodeCommon, ErrorTag, ErrorBadInputPath)
			return
		}
	}

	// set working directory path
	c.workPath = c.InputPath

	// check for input path directory profile
	fi, err := os.Stat(c.InputPath)
	if err != nil {
		c.exit(ErrorCodeCommon, ErrorTag, err.Error())
		return
	}

	if !fi.Mode().IsDir() {
		c.exit(ErrorCodeCommon, ErrorTag, ErrorInputPathNotDirectory)
	}
}

func (c *App) validateOutputPath() {
	var err error

	// check output type
	switch {
	case c.OutputPath == Terminal:
		return
	case c.OutputPath == "":
		c.exit(ErrorCodeCommon, ErrorTag, ErrorMissingOutputPath)
		return
	}

	// output is not terminal, check the directory path
	if !filepath.IsAbs(c.OutputPath) {
		c.OutputPath, err = filepath.Abs(c.OutputPath)
		if err != nil || c.testConfig[testSimulateFailedABSOutput] {
			c.exit(ErrorCodeCommon, ErrorTag, ErrorBadOutputPath)
			return
		}
	}

	// checkout directory path integrity
	fi, err := os.Stat(c.OutputPath)

	if c.testConfig[testSimulateBadStatOutput] {
		err = fmt.Errorf(errorSimulateBadStatOutput)
	}

	switch {
	case err == nil && fi.Mode().IsRegular():
		c.exit(ErrorCodeCommon, ErrorTag, ErrorOutputPathIsAFile)
	case os.IsNotExist(err), err == nil && fi.Mode().IsDir():
		// do nothing since this the the output we want
	default:
		c.exit(ErrorCodeCommon, ErrorTag, err.Error())
	}
}

func (c *App) validateTemplatePath() {
	switch {
	case c.OutputPath == Terminal && c.TemplatePath == "":
		// bail since template in terminal is optional
		return
	case c.OutputPath == Terminal:
		// pass
	case c.Filename != "" && c.TemplatePath == "":
		c.exit(ErrorCodeCommon, ErrorTag, ErrorMissingTemplatePath)
		return
	}

	// validate TemplatePath is a file
	fi, err := os.Stat(c.TemplatePath)

	switch {
	case err == nil && fi.Mode().IsRegular():
		// do nothing since this is a pass case
	case err == nil && fi.Mode().IsDir():
		c.exit(ErrorCodeCommon, ErrorTag, ErrorTemplatePathIsADir)
	default:
		c.exit(ErrorCodeCommon,
			ErrorTag,
			ErrorBadTemplatePath+" : "+err.Error())
	}
}

func (c *App) validateFilename() {
	if c.Filename != "" {
		return
	}

	// set default to index.txt if output is not terminal and filename is
	// empty.
	if c.Filename == "" && c.OutputPath != Terminal {
		c.Filename = DefaultFilename
	}
}

func (c *App) generateDocData() {
	// exit if exit code is not 0
	if c.ExitCode != ErrorCodeNone {
		return
	}

	// process all input paths including its recursiveness
	targets := []string{}
	_ = filepath.Walk(c.InputPath,
		func(path string, fi os.FileInfo, err error) error {
			if path == c.InputPath {
				targets = append(targets, path)
				return nil
			}

			if fi == nil || !c.recursive {
				return nil
			}

			if fi.Mode().IsDir() {
				targets = append(targets, path)
			}

			return nil
		},
	)

	// generate doc data based on input lists
	for _, path := range targets {
		c.generateRawData(path,
			c.workPath,
			c.OutputPath,
			c.Filename,
			c.testConfig)
	}

	if len(c.Targets) == 0 {
		c.exit(ErrorCodeCommon, ErrorTag, ErrorInputPathWithoutGo)
		return
	}
}

func (c *App) renderDocData() {
	// bail or stay conditions
	switch {
	case c.ExitCode != ErrorCodeNone:
		return
	case c.OutputPath == Terminal && c.testConfig[testMode]:
	case c.WorkMode == AppMode:
	default:
		return
	}

	// If it is not terminal or in test-mode, clean-up output directory
	if c.OutputPath != Terminal || c.testConfig[testMode] {
		os.RemoveAll(c.OutputPath)
		_ = os.MkdirAll(c.OutputPath, os.ModePerm)
	}

	// render each doc data
	for _, data := range c.Targets {
		c.renderDoc(data,
			c.OutputPath,
			c.TemplatePath,
			c.Filename,
			c.Width,
			c.testConfig,
		)
	}
}

// |---------------------------------------------------------|
// |-- elemental private functions for parallel executions --|
// |---------------------------------------------------------|

func (c *App) generateRawData(path string,
	workPath string,
	outputPath string,
	filename string,
	testConfig map[int]bool) {
	data := NewData()
	err := data.parse(path, workPath, outputPath, filename, testConfig)

	switch {
	case err == nil:
		c.Targets = append(c.Targets, data)
	case errorIs(err, ErrorConfigureDocData):
		return
	default:
		c.printStatus(WarningTag, err.Error())
	}
}

func (c *App) renderDoc(data *Data,
	outputPath string,
	templatePath string,
	filename string,
	width uint,
	testConfig map[int]bool) {
	f := &formatter{
		pkg:          data,
		outputPath:   outputPath,
		templatePath: templatePath,
		filename:     filename,
		width:        width,
		testConfig:   testConfig,
	}

	err := f.openWriter()
	if err != nil {
		c.printStatus(WarningTag, err.Error())
		return
	}

	f.renderPackage()
	f.closeWriter()
}

func (c *App) printStatus(tag string, format string, a ...interface{}) {
	var s *bytes.Buffer

	port := io.Writer(os.Stderr)

	if c.WorkMode != AppMode || c.testConfig[testMode] {
		s = &bytes.Buffer{}
		port = io.Writer(s)
	}

	fmt.Fprintf(port, "[ "+tag+" ] "+format, a...)

	if c.WorkMode == LibraryMode || c.testConfig[testMode] {
		c.Log = append(c.Log, s.String())
	}
}

//nolint:unparam
func (c *App) exit(code int, tag string, message string) {
	if message != "" {
		c.printStatus(tag, message)
	}

	c.exitNow(code)
}
