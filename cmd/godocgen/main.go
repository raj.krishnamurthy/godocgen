// Package main from cmd/godocgen is to build the godocgen binary.
//
// The package is to facilitate `Go Get` command for godocgen.
package main

import (
	"gitlab.com/zoralab/godocgen/api/cmdline"
)

func main() {
	app := &cmdline.CMD{}
	app.ParseArgs()
}
