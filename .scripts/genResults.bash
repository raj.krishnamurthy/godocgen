#!/bin/bash

if [ ! -f "./go.mod" ]; then
	1>&2 echo "[ ERROR ] not in repo root directory."
	return 1
fi

filename="_index"
testpkg_path="./internal/testpkg"
result_path="./test/results"
terminal_result_path="${result_path}/terminal.txt"


# generate markdown results
go run cmd/godocgen/main.go \
	--output "${result_path}/md" \
	--input "${testpkg_path}/..." \
	--filename="${filename}.md" \
	--template=.godocgen/templates/md


# generate text results
go run cmd/godocgen/main.go \
	--output "${result_path}/txt" \
	--input "${testpkg_path}/..." \
	--filename="${filename}.txt" \
	--template=.godocgen/templates/txt


# generate terminal results
go run cmd/godocgen/main.go \
	--output terminal \
	--input "${testpkg_path}/..." \
	> "$terminal_result_path"
