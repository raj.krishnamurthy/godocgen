// Package cmdline is the command line API for Godocgen.
//
// This is for other Go command application to have Godocgen's compatible
// command line interfaces (CLI).
package cmdline

import (
	"fmt"

	"gitlab.com/zoralab/cerigo/os/args"
	"gitlab.com/zoralab/godocgen"
)

// CMD is the API structure for running godocgen command line interface API
//
// It is safe to create CMD object using the conventional `&CMD{}` method.
type CMD struct {
	testMode bool
}

// ParseArgs is the command line interface arguments processor.
//
// It processes all the necessary arguments before executing `App.Run()`. This
// function also holds the command lines parameters descriptions for Godocgen
// program.
//
// ParseArgs reads from `os.Args`, via ZORALab's Cerigo's `os/args` library.
func (cmd *CMD) ParseArgs() {
	c := godocgen.NewApp()
	m := args.NewManager()
	action := ""

	// create setup flag managers
	m.Name = "Go Docs Generator"
	m.Description = `
generate go packages' documentations in a proper output like Markdown files
and etc.`
	m.Version = godocgen.VERSION
	m.Examples = []string{
		`simple project: $ godocgen --output ./docs/en-us/docs \
				--input ./... \
				--template ./.godocgen/templates/md`,
		`single package: $ godocgen --output ./docs/en-us/docs \
				--input . \
				--template ./.godocgen/templates/md`,
	}
	m.ShowFlagExamples = true

	// add arguments flags
	_ = m.Add(&args.Flag{
		Name:       "Filename",
		Label:      []string{"--filename"},
		ValueLabel: "STRING",
		Value:      &c.Filename,
		Help: `
the filename for generating the output file. Default is index.txt.`,
		HelpExamples: []string{
			"$ godocgen --filename _index.md ...",
		},
	})

	_ = m.Add(&args.Flag{
		Name:  "Help",
		Label: []string{"-h", "--help", "help"},
		Value: &action,
		Help:  "to call for help",
		HelpExamples: []string{
			"$ godocgen -h",
			"$ godocgen --help",
			"$ godocgen help",
		},
	})

	_ = m.Add(&args.Flag{
		Name:  "Version",
		Label: []string{"-v", "--version", "version"},
		Value: &action,
		Help:  "to get current version",
		HelpExamples: []string{
			"$ godocgen -v",
			"$ godocgen --version",
			"$ godocgen version",
		},
	})

	_ = m.Add(&args.Flag{
		Name:       "Output Path",
		Label:      []string{"-o", "--output"},
		ValueLabel: "PATH",
		Value:      &c.OutputPath,
		Help: `
to specify output directory. see examples for available options.`,
		HelpExamples: []string{"",

			"$ godocgen -o /path/to/dir ...",
			"$ godocgen --output /path/to/dir ...",
			"$ godocgen -o " + godocgen.Terminal + " ...",
			"$ godocgen --output " +
				godocgen.Terminal +
				" ...",
		},
	})

	_ = m.Add(&args.Flag{
		Name:       "Package Path(s)",
		Label:      []string{"-i", "--input"},
		ValueLabel: "PATH(/...)",
		Value:      &c.InputPath,
		Help: `
specify the package directory for scribing. Append '/...' at the end for
recursiveness.`,
		HelpExamples: []string{
			"$ godocgen -i /path/to/input",
			"$ godocgen --input /path/to/input",
			"$ godocgen -i /path/to/input/...",
			"$ godocgen --input /path/to/input/...",
		},
	})

	_ = m.Add(&args.Flag{
		Name:       "Template Path",
		Label:      []string{"-t", "--template"},
		ValueLabel: "PATH(/...)",
		Value:      &c.TemplatePath,
		Help: `
specify the template file for renderer to based on. The template must be
compliant to go template format.`,
		HelpExamples: []string{
			"$ godocgen --template /path/to/file ...",
		},
	})

	// process arguments as per actions
	m.Parse()

	switch action {
	case "-h", "--help", "help":
		fmt.Printf("%s", m.PrintHelp())
	case "-v", "--version", "version":
		fmt.Printf("%s\n", godocgen.VERSION)
	default:
		c.WorkMode = godocgen.AppMode
		if cmd.testMode {
			c.WorkMode = godocgen.MockMode
		}

		c.Run()
	}
}
