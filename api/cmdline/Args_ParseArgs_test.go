package cmdline

import (
	"testing"
)

func TestParseArgs(t *testing.T) {
	sl := getTestScenarios()

	for i, s := range sl {
		// prepare
		th := s.prepareTHelper(t)
		s.simulateInput()

		// test
		cmd := &CMD{testMode: true}
		cmd.ParseArgs()

		// verify
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.log(th, map[string]interface{}{})
		th.Conclude()
	}
}
