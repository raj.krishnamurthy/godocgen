package godocgen

import (
	"fmt"
	"strings"
)

// Error constants are the values used for error identifications via
// application programming.
const (
	// ErrorTag is the error message tag for printing out error messages.
	ErrorTag = "ERROR"

	// WarningTag is the warning message tag for printing out less critical
	// error messages.
	WarningTag = "WARNING"

	// ErrorCodeNone is the error code that has no errors.
	ErrorCodeNone = 0

	// ErrorCodeCommon is the error code for common errors.
	ErrorCodeCommon = 1
)

// Error messages are the string values used for error object text output.
const (
	// ErrorBadInputPath is the error message for a completely bad and
	// unusable input path.
	ErrorBadInputPath = "bad input path"

	// ErrorBadOutputPath is the error message for a completely bad and
	// unusable output path.
	ErrorBadOutputPath = "bad output path"

	// ErrorBadTemplatePath is the error message for a completely bad and
	// unusable template path.
	ErrorBadTemplatePath = "bad template path"

	// ErrorConfigureDocData is the error message for unable to configure
	// package's raw DocData
	ErrorConfigureDocData = "unable to configure raw DocData"

	// ErrorMissingInputPath is the error message for completely missing
	// input path.
	ErrorMissingInputPath = "missing input path"

	// ErrorMissingOutputPath is the error message for completely missing
	// output path.
	ErrorMissingOutputPath = "missing output path"

	// ErrorMissingTemplatePath is the error message for completely missing
	// template path.
	ErrorMissingTemplatePath = "missing template path"

	// ErrorInputPathNotDirectory is the error message for the given input
	// path is not a directory.
	ErrorInputPathNotDirectory = "given input path is not a directory"

	// ErrorInputPathWithoutGo is the error message for the given input
	// path does not contain any directory.
	ErrorInputPathWithoutGo = "given input path has no go packages"

	// ErrorOutputPathIsAFile is the error message for the given output
	// path is a file and is not ready for output usage.
	ErrorOutputPathIsAFile = "output path is a file"

	// ErrorOutputPermission is the error message for unable
	// to read/write into output.
	ErrorOutputPermission = "unable to read/write to output"

	// ErrorTemplatePathIsADir is the error message for the given output
	// path is a directory and is not a file for template rendering.
	ErrorTemplatePathIsADir = "template path is a directory"

	// ErrorInTemplate is the error message for a given template having
	// its internal errors.
	ErrorInTemplate = "template is having error"

	// ErrorUnknownOutputFormat is the error message for unidentifiable
	// filetype based on the given filename.
	ErrorUnknownOutputFormat = "unknown filetype from the given filename"

	// ErrorUnknownWorkMode is the error message for unidentifiable
	// work mode.
	ErrorUnknownWorkMode = "unknown work mode"
)

func createError(errMessage string, a interface{}) error {
	return fmt.Errorf(errMessage+" : %v", a)
}

func errorIs(err error, errMessage string) bool {
	return strings.Contains(err.Error(), errMessage)
}
