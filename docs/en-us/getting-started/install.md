<!--
+++
date = "2020-03-16T17:00:44+08:00"
title = "Install Godocgen"
description = """
This section covers how to install Godocgen as a user into his/her operating
system.
"""
keywords = ["install"]
authors = ["Godocgen Team", "Holloway Chew Kean Ho"]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "A) Getting Started"
# name = "Install Godocgen"
weight = 1
+++
-->

# Install GodocGen
Godocgen has a number of ways to install as a program in your operating system.
Let's look at these supported methods.

## Go Get
To install via `go get`, you simply do `go get` against a release branch
version:

```bash
$ go get gitlab.com/zoralab/godocgen/cmd/godocgen@releases-VERSION
```

This will install the binary automatically inside your `$GOBIN` (or
`$GOPATH/bin` path).

> **NOTE**: we decided to use *branching* over *tagging* mainly because the
> current `Go Get` enforces version folder implementation.
>
> This enforcement is enough to create messed up code base, unnecessary codes
> duplications, and complicated the user guides (inconsistent patterns). Hence,
> It is better to use protective *branches* instead.



### Supported Versions
These are the supported `Go Get` versions:

```bash
$ go get gitlab.com/zoralab/godocgen/cmd/godocgen@releases-v0.0.2
$ go get gitlab.com/zoralab/godocgen/cmd/godocgen@releases-v0.0.1
```




## DEB Package (Coming Soon)
The DEB package server hosting is planned and will be supported soon.


## RPM Package (Coming Soon)
The RPM package server hosting is planned and will be supported soon.
