<!--
+++
date = "2020-03-16T17:59:43+08:00"
title = "Sponsoring"
description = """
This section covers the financial sponsoring to the Godocgen Project. A small
financial sponsorship really helps us a lot in reality.
"""
keywords = ["sponsor", "Godocgen", "contribute"]
authors = [ "Godocgen Team" ]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Y) Projects"
weight = 4
+++
-->

# Sponsoring
Thank you for your interest in sponsoring Godocgen. Currently Godocgen is under
the following program for its development and maintenances:

1. [Shepard Program](https://www.zoralab.com/stores/services/shepherd-program) from ZORALab Enterprise

You can help us by sponsoring a one-time small tips (e.g. a cup of coffee)
or a monthly sponsorship through our selected payment channels. By sponsoring
us, you help us to press on development useful open-source softwares for the
benefits of all.


## What You'll Get
As a returning favor for those who help us via subscription sponsorship, we
will do:

1. A customized `300px x 100px` badge designed and placed in this project's
[Homepage]({{< absLangLink "/" >}}) and
[Contributors]({{< absLangLink "/projects/contributors" >}}) pages.
2. Mentioned in development and CHANGELOG!


## Read Terms and Conditions
Be sure to read through the [Terms and Conditions](#1-terms-and-conditions)
below.

**Once you perform a payment transaction, you're automatically agreed to our
terms and conditions**.


## Payment
You can use any of our selected payment channels.

### One Time
This is the one-time only payment. It is useful for those who want to tip us
once in a blue moon.

{{<  sponsors/PayPalME "zoralab" "USD" "8" "15" "25" >}}

### Subscription
This is the subscription based payment. It is useful for those who want to
continuously supports us.

{{< renderHTML html >}}
<form target="_top">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="USCPNQVJKSJPA">
	<input type="hidden" name="on0" value="Select Plan">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="on2" value="Project">
	<input type="hidden" name="os2" value="Granatum">

	<fieldset>
		<label for="software-sponsorship-one-time-subscription">
			Select Options
		</label>
		<select id="software-sponsorship-one-time-subscription"
			name="os0" required>
			<option value="USD8">
				USD8 : $8.00 USD - monthly
			</option>
			<option value="USD15">
				USD15 : $15.00 USD - monthly
			</option>
			<option value="USD20">
				USD20 : $20.00 USD - monthly
			</option>
		</select>
	</fieldset>

	<fieldset>
		<label for="software-sponsorship-subscription-full-name">
			Full Name
		</label>
		<input type="hidden" name="on1" value="Full Name" />
		<input id="software-sponsorship-subscription-full-name"
			type="text"
			name="os1"
			maxlength="200"
			placeholder="John S. Smith"
			required />
	</fieldset>

	<fieldset style="display: inline-block">
		<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=6NQSE3QY32KPS">
			Unsubscribe
		</a>
		<input type="submit" formmethod="post" formaction="https://www.paypal.com/cgi-bin/webscr" class="pinpoint" value="Subscribe" />
	</fieldset>
</form>
{{< /renderHTML >}}


## Terms and Conditions
We know you guys will probably just skip or skim through this section in
seconds, so we make it easier by summarizing it for you read through.

```
The following terms and conditions (these `Terms and Conditions`) have been
established by ZORALab Enterprise to set out the rights and obligations of a
sponsor of the above specified programme and become effective with
ZORALab Enterprise’s written confirmation of a sponsor application.

'We', 'our' or 'us' refers to ZORALab Enterprise.

'Project', 'project' refers to the software repository where this
Terms and Conditions document attached, deposited or linked within.

'Contributors' refers to the list of entity including us that contributes
our efforts into the project, such as code changes, issue creation and
management, Project management and releases.
```

### Change of Terms and Conditions
We will update the Terms and Conditions from time to time. Before applying
the new changes, we'll inform you via public announcement or emailing you.

```
We may sometimes make changes to these terms. If the material changes adversely
affect your rights under these terms, we will announce through official posting
on the website or sending you an email prior to the changes coming into effect.

Continuing to sponsor after the changes became effective means you accept the
new terms.
```

### Sponsorship Registration
By performing a payment to our sponsor account, you automatically agreed to
all the terms and conditions listed here.

```
By performing the payment via our published payment gateway, you are
automatically agreed and signed to all the terms and conditions listed in this
Terms and Conditions.
```

### Sponsor Materials
You keep the ownership of the content but give us the permission and right to
use it in the project you're sponsoring.

```
You keep full ownership over the materials created for the sponsorship but to
publish them in this repository's website and mentions, we need a license from
you.

By becoming the sponsor, you grant us a royalty-free, perpetual, irrevocable,
non-exclusive, sublicensable, worldwide license to use, reproduce, distribute,
perform, publicly display or prepare derivative works of your content.

The purpose of this license is to allow us to operate the Project, and to
promote your content on the Project. We and the Project may not use the content
posted by the creators in any way not authorized by the creator.
```

### Materials
Respect your materials with integrity by:

1. Not stealing from others, materials
2. Not containing nudity
3. Not legally offensive
4. Not promoting violent, criminal or hateful behaviour

This includes artwork as as well.

```
You may not post content that infringes on others' intellectual property or
proprietary rights, that displays genitals, focusing in on fully exposed
buttocks, or female breasts includind/excluding the nipple.
```

### Our Materials
You can use our contents from the Project within its permitted license; but
can't use it anywhere else without our written permission.

If unsure, ask us!

```
Contents we created in this Project is protected by copyright, licenses,
trademarks and trade secret laws. Some example are the text on the Project or
our website, logo, and the Project codebase.

You may not otherwise use, reproduce, distribute, perform, publicly display
or prepare derivative works of the Project contents outside of its permitted
licenses unless we give you permission in writing. Please ask if you have any
questions.
```

### Indemnity
If we got sued because of you, you have to help us to pay for it.

```
You will indemnify us from all losses and liabilities, including legal fees,
that arise from these terms or relate to your content. We reserve the right to
exclusive control over the defense of a claim covered by this clause. If we use
this right then you will help us in our defense.

Your obligation to indemnify under this clause also applies to our
affiliates, officers, directors, employees, agents,
third party service providers and the Project Contributors.
```

#### Force Majeure and Termination
If there is a terminal distruption by the Force, we'll work together to find
other ways to keep continue/cancel the sponsorship.

```
In the event of fire, strike, civil commotion, act of terrorism, act of God, or
other force majeure making it impossible or impractical for us to operate the
sponsorship, we shall not be held in breach of its sponsorship obligations.

In such case, the sponsorship efforts shall be suspended by joint efforts from
you and us to find alternative ways of executing the sponsorship.

We may, at its sole discretion, terminate the sponsorship at any time by
returning pro rata any sponsorship fees paid by such sponsor for the remainder
of the Sponsorship year and you shall not be entitled to claim any damages.
```

### Laws
Any disputes with us must be resolved in Kuala Lumpur under Malaysia Law.

```
Malaysia law, excluding its conflict of law provisions, governs these terms for
the sponsorship policy. If a lawsuit does arise, both parties consent to the
exclusive jurisdiction and venue of the courts located in Kuala Lumpur,
Malaysia.
```

### Contact
If you have any questions, please email to us at
[legal@zoralab.com](email:legal@zoralab.com).
