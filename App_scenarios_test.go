package godocgen

func testAppScenarios() []testScenario {
	return []testScenario{
		{
			UID:      1,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:        true,
				useProperInputPath:    true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      2,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:            true,
				useProperInputPath:    true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      3,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:           true,
				useProperInputPath:    true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      4,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:        true,
				useProperInputPath:    true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      5,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. non-go directory input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:        true,
				useNonGoInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      6,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. non-go directory input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:            true,
				useNonGoInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      7,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. non-go directory input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:           true,
				useNonGoInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      8,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. non-go directory input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:        true,
				useNonGoInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      9,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:        true,
				useEmptyInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      10,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:            true,
				useEmptyInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      11,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:           true,
				useEmptyInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      12,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:        true,
				useEmptyInputPath:     true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      13,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:        true,
				useBadInputPath:       true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      14,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:            true,
				useBadInputPath:       true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      15,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:           true,
				useBadInputPath:       true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      16,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:        true,
				useBadInputPath:       true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      17,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:        true,
				useFileInputPath:      true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      18,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:            true,
				useFileInputPath:      true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      19,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:           true,
				useFileInputPath:      true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      20,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:        true,
				useFileInputPath:      true,
				useTerminalOutputPath: true,
				useNoFilename:         true,
				useNoTemplatePath:     true,
				useDefaultWidth:       true,
			},
		}, {
			UID:      21,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      22,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      23,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      24,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      25,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      26,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useEmptyInputPath:       true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      27,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useEmptyInputPath:       true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      28,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. empty input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useEmptyInputPath:       true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      29,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useBadInputPath:         true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      30,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useBadInputPath:         true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      31,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useBadInputPath:         true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      32,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. bad input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useBadInputPath:         true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      33,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useFileInputPath:        true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      34,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useFileInputPath:        true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      35,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useFileInputPath:        true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      36,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. file type input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useFileInputPath:        true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      37,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. simulate bad absolute input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
				simulateBadABSInputPath: true,
			},
		}, {
			UID:      38,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. simulate bad absolute input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
				simulateBadABSInputPath: true,
			},
		}, {
			UID:      39,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. simulate bad absolute input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
				simulateBadABSInputPath: true,
			},
		}, {
			UID:      40,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. simulate bad absolute input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
				simulateBadABSInputPath: true,
			},
		}, {
			UID:      41,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. empty output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useEmptyOutputPath:      true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      42,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. empty output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useEmptyOutputPath:      true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      43,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. empty output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useEmptyOutputPath:      true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      44,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. empty output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useEmptyOutputPath:      true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      45,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. terminal output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useTerminalOutputPath:   true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      46,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      47,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      48,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      49,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed ABS output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useNoTemplatePath:        true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      50,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed absolute output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:               true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useNoTemplatePath:        true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      51,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed absolute output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:              true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useNoTemplatePath:        true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      52,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed absolute output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useNoTemplatePath:        true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      53,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. file type output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useFileOutputPath:       true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      54,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. file type output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useFileOutputPath:       true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      55,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. file type output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useFileOutputPath:       true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      56,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. file type output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useFileOutputPath:       true,
				useNoFilename:           true,
				useNoTemplatePath:       true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      57,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulate bad stat output path is given.
3. no filename is given.
4. no template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:            true,
				useProperInputPath:        true,
				processInputRecursively:   true,
				useProperOutputPath:       true,
				useNoFilename:             true,
				useNoTemplatePath:         true,
				useDefaultWidth:           true,
				simulateBadStatOutputPath: true,
			},
		}, {
			UID:      58,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulate bad stat output path is given.
3. no filename is given.
4. no template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:                true,
				useProperInputPath:        true,
				processInputRecursively:   true,
				useProperOutputPath:       true,
				useNoFilename:             true,
				useNoTemplatePath:         true,
				useDefaultWidth:           true,
				simulateBadStatOutputPath: true,
			},
		}, {
			UID:      59,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulate bad stat output path is given.
3. no filename is given.
4. no template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:               true,
				useProperInputPath:        true,
				processInputRecursively:   true,
				useProperOutputPath:       true,
				useNoFilename:             true,
				useNoTemplatePath:         true,
				useDefaultWidth:           true,
				simulateBadStatOutputPath: true,
			},
		}, {
			UID:      60,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulate bad stat output path is given.
3. no filename is given.
4. no template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:            true,
				useProperInputPath:        true,
				processInputRecursively:   true,
				useProperOutputPath:       true,
				useNoFilename:             true,
				useNoTemplatePath:         true,
				useDefaultWidth:           true,
				simulateBadStatOutputPath: true,
			},
		}, {
			UID:      61,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. directory template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:               true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useDirectoryTemplatePath: true,
				useDefaultWidth:          true,
			},
		}, {
			UID:      62,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. directory template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:              true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useDirectoryTemplatePath: true,
				useDefaultWidth:          true,
			},
		}, {
			UID:      63,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. directory template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useDirectoryTemplatePath: true,
				useDefaultWidth:          true,
			},
		}, {
			UID:      64,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed ABS output path is given.
3. no filename is given.
4. directory template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useDirectoryTemplatePath: true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      65,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. bad template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useBadTemplatePath:      true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      66,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. bad template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useBadTemplatePath:      true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      67,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. bad template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useBadTemplatePath:      true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      68,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed ABS output path is given.
3. no filename is given.
4. bad template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useBadTemplatePath:       true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      69,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      70,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      71,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. no filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useNoFilename:           true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      72,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed ABS output path is given.
3. no filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useNoFilename:            true,
				useProperTemplatePath:    true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      73,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. bad filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useBadFilename:          true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      74,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. bad filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useBadFilename:          true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      75,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. bad filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useBadFilename:          true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      76,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. simulated failed ABS output path is given.
3. bad filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:           true,
				useProperInputPath:       true,
				processInputRecursively:  true,
				useProperOutputPath:      true,
				useBadFilename:           true,
				useProperTemplatePath:    true,
				useDefaultWidth:          true,
				simulateBadABSOutputPath: true,
			},
		}, {
			UID:      77,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      78,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      79,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      80,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      81,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on library mode.
6. use zero width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useZeroWidth:            true,
			},
		}, {
			UID:      82,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on mock mode.
6. use zero width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useZeroWidth:            true,
			},
		}, {
			UID:      83,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use zero width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useZeroWidth:            true,
			},
		}, {
			UID:      84,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on app mode.
6. use zero width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useZeroWidth:            true,
			},
		}, {
			UID:      85,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
7. simulate generator error.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
				simulateGeneratorError:  true,
			},
		}, {
			UID:      86,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
7. simulate generator error.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
				simulateGeneratorError:  true,
			},
		}, {
			UID:      87,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
7. simulate generator error.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
				simulateGeneratorError:  true,
			},
		}, {
			UID:      88,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
7. simulate generator error.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
				simulateGeneratorError:  true,
			},
		}, {
			UID:      89,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
7. simulate canva creation error.
`,
			Switches: map[string]bool{
				useLibraryMode:              true,
				useProperInputPath:          true,
				processInputRecursively:     true,
				useProperOutputPath:         true,
				useProperFilename:           true,
				useProperTemplatePath:       true,
				useDefaultWidth:             true,
				simulateFailedCanvaCreation: true,
			},
		}, {
			UID:      90,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
7. simulate canva creation error.
`,
			Switches: map[string]bool{
				useMockMode:                 true,
				useProperInputPath:          true,
				processInputRecursively:     true,
				useProperOutputPath:         true,
				useProperFilename:           true,
				useProperTemplatePath:       true,
				useDefaultWidth:             true,
				simulateFailedCanvaCreation: true,
			},
		}, {
			UID:      91,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
7. simulate canva creation error.
`,
			Switches: map[string]bool{
				useUnknownMode:              true,
				useProperInputPath:          true,
				processInputRecursively:     true,
				useProperOutputPath:         true,
				useProperFilename:           true,
				useProperTemplatePath:       true,
				useDefaultWidth:             true,
				simulateFailedCanvaCreation: true,
			},
		}, {
			UID:      92,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
7. simulate canva creation error.
`,
			Switches: map[string]bool{
				useAppMode:                  true,
				useProperInputPath:          true,
				processInputRecursively:     true,
				useProperOutputPath:         true,
				useProperFilename:           true,
				useProperTemplatePath:       true,
				useDefaultWidth:             true,
				simulateFailedCanvaCreation: true,
			},
		}, {
			UID:      93,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
7. simulate file creation error.
`,
			Switches: map[string]bool{
				useLibraryMode:             true,
				useProperInputPath:         true,
				processInputRecursively:    true,
				useProperOutputPath:        true,
				useProperFilename:          true,
				useProperTemplatePath:      true,
				useDefaultWidth:            true,
				simulateFailedFileCreation: true,
			},
		}, {
			UID:      94,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
7. simulate file creation error.
`,
			Switches: map[string]bool{
				useMockMode:                true,
				useProperInputPath:         true,
				processInputRecursively:    true,
				useProperOutputPath:        true,
				useProperFilename:          true,
				useProperTemplatePath:      true,
				useDefaultWidth:            true,
				simulateFailedFileCreation: true,
			},
		}, {
			UID:      95,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
7. simulate file creation error.
`,
			Switches: map[string]bool{
				useUnknownMode:             true,
				useProperInputPath:         true,
				processInputRecursively:    true,
				useProperOutputPath:        true,
				useProperFilename:          true,
				useProperTemplatePath:      true,
				useDefaultWidth:            true,
				simulateFailedFileCreation: true,
			},
		}, {
			UID:      96,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
7. simulate file creation error.
`,
			Switches: map[string]bool{
				useAppMode:                 true,
				useProperInputPath:         true,
				processInputRecursively:    true,
				useProperOutputPath:        true,
				useProperFilename:          true,
				useProperTemplatePath:      true,
				useDefaultWidth:            true,
				simulateFailedFileCreation: true,
			},
		}, {
			UID:      97,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. markdown filename is given.
4. markdown template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useMarkdownFilename:     true,
				useMarkdownTemplatePath: true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      98,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. markdown filename is given.
4. markdown template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useMarkdownFilename:     true,
				useMarkdownTemplatePath: true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      99,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. markdown filename is given.
4. markdown template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useMarkdownFilename:     true,
				useMarkdownTemplatePath: true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      100,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. markdown filename is given.
4. markdown template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useMarkdownFilename:     true,
				useMarkdownTemplatePath: true,
				useDefaultWidth:         true,
			},
		},
	}
}

/* for copy and paste happy path

		}, {
			UID:      77,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on library mode.
6. use default width.
`,
			Switches: map[string]bool{
				useLibraryMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      78,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on mock mode.
6. use default width.
`,
			Switches: map[string]bool{
				useMockMode:             true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      79,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on unknown mode.
6. use default width.
`,
			Switches: map[string]bool{
				useUnknownMode:          true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},
		}, {
			UID:      80,
			TestType: testRun,
			Description: `
App.Run() is working properly when:
1. proper directory input path with recursiveness is given.
2. proper output path is given.
3. proper filename is given.
4. proper template path is given.
5. running on app mode.
6. use default width.
`,
			Switches: map[string]bool{
				useAppMode:              true,
				useProperInputPath:      true,
				processInputRecursively: true,
				useProperOutputPath:     true,
				useProperFilename:       true,
				useProperTemplatePath:   true,
				useDefaultWidth:         true,
			},

*/
