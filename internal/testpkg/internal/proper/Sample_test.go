package proper

import (
	"testing"
)

func TestSample(t *testing.T) {
	s := Sample()

	t.Logf("Sample() returns %s", s)

	if s == "" {
		t.Errorf("Sample() is returning weird results.")
	}
}
