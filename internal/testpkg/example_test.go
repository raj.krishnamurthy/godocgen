package testpkg_test

import (
	"fmt"
)

// How to demonstrate package first example.
func Example() {
	fmt.Println("This is the package main example")
	// Output: This is the package main example
}
