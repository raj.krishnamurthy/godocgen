package renderer

const (
	goCommentSymbol       = "// "
	goCommentSymbolLength = uint(len(goCommentSymbol))
	tabSize               = uint(8)
	tab                   = "\t"
)
