package renderer

import (
	"io"
	"strings"
	"text/template"
	"time"

	"gitlab.com/zoralab/cerigo/strings/strhelper"
)

type textParser struct {
	buffer       io.Writer
	width        uint
	templatePath string
	engine       *template.Template
}

func (r *textParser) reset() error {
	var err error

	if r.width == 0 {
		r.width = 70
	}

	r.engine, err = template.ParseFiles(r.templatePath)
	if err != nil {
		return err
	}

	return nil
}

func (r *textParser) renderHead(name string,
	synopsis string,
	relativeName string) {
	t := time.Now()

	data := map[string]string{
		HeaderLabel:        name,
		SynopsisLabel:      synopsis,
		NameLabel:          relativeName,
		HugoTimestampLabel: t.Format(time.RFC3339),
		TimestampLabel:     t.Format(time.RFC1123Z),
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateHead, data)
}

func (r *textParser) renderTitle(pkg string, name string) {
	data := map[string]string{
		HeaderLabel: pkg,
		NameLabel:   name,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateTitle, data)
}

func (r *textParser) renderHeader(name string) {
	data := map[string]string{
		NameLabel: name,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateHeader, data)
}

func (r *textParser) renderSynopsis(synopsis string) {
	data := map[string]string{
		SynopsisLabel: synopsis,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateSynopsis, data)
}

func (r *textParser) renderDeclGroup(name string) {
	data := map[string]string{
		NameLabel: name,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateDeclGroup, data)
}

func (r *textParser) renderDeclElements(header string, value string) {
	sh := strhelper.Styler{}
	actualWidth := r.width - goCommentSymbolLength - tabSize
	hl := sh.ContentWrap(header, actualWidth, strhelper.CharUNIXNewLine)
	hla := []string{}

	for _, v := range hl {
		if v == "" || v == strhelper.CharUNIXNewLine {
			continue
		}

		hla = append(hla, goCommentSymbol+v)
	}

	h := strings.Join(hla, strhelper.CharUNIXNewLine+tab)

	data := map[string]string{
		HeaderLabel: h,
		ValueLabel:  value,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateDeclElements, data)
}

func (r *textParser) renderEndDeclGroup() {
	_ = r.engine.ExecuteTemplate(r.buffer, TemplateEndDeclGroup, nil)
}

func (r *textParser) renderFunction(header string,
	receiver string,
	value string) {
	value = strings.Replace(value, "\n", " ", -1)
	value = strings.Replace(value, "\t", "", -1)

	data := map[string]string{
		HeaderLabel:      header,
		HasReceiverLabel: receiver,
		ValueLabel:       value,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateFunction, data)
}

func (r *textParser) renderCode(codes []string) {
	data := map[string][]string{
		CodesLabel: codes,
	}
	_ = r.engine.ExecuteTemplate(r.buffer, TemplateCode, data)
}

func (r *textParser) renderNewLine() {
	_ = r.engine.ExecuteTemplate(r.buffer, TemplateNewLine, nil)
}

func (r *textParser) renderImportPath(path string) {
	data := map[string]string{
		ValueLabel: path,
	}
	_ = r.engine.ExecuteTemplate(r.buffer, TemplateImportPath, data)
}

func (r *textParser) renderExample(name string,
	suffix string,
	doc string,
	codes []string,
	output string) {
	data := map[string]interface{}{
		NameLabel:     name,
		HeaderLabel:   suffix,
		SynopsisLabel: doc,
		CodesLabel:    codes,
		ValueLabel:    output,
	}

	_ = r.engine.ExecuteTemplate(r.buffer, TemplateExample, data)
}
