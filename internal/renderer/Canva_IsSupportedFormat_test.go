package renderer

import (
	"testing"
)

func TestIsSupportedFormat(t *testing.T) {
	scenarios := testScenarios()

	for i, s := range scenarios {
		if s.TestType != testIsSupportedFormat {
			continue
		}

		// prepare
		th := s.PrepareTHelper(t)
		filepath, expect := s.GenerateSetup()

		// test
		doctype := IsSupportedFormat(filepath)

		// verify
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.Log(th, map[string]interface{}{
			"Filepath":          filepath,
			"Doc Type":          doctype,
			"expected Doc Type": expect,
		})
		th.Conclude()
	}
}
