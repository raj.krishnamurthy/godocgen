package godocgen

import (
	"testing"
)

func TestRun(t *testing.T) {
	scenarios := testAppScenarios()

	for i, s := range scenarios {
		if s.TestType != testRun {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		s.PrepareTestEnvironment()

		app := NewApp()
		s.Configure(app)

		// test
		app.Run()

		// verify
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.scanForInputPathError(th, app)
		s.scanForOutputPathError(th, app)
		s.scanForTemplatePathError(th, app)
		s.scanForOutput(th, app)
		s.log(th, map[string]interface{}{
			"Output Path":   app.OutputPath,
			"Input Path":    app.InputPath,
			"Filename":      app.Filename,
			"Width":         app.Width,
			"Log":           app.Log,
			"ExitCode":      app.ExitCode,
			"Template Path": app.TemplatePath,
			"Targets":       app.Targets,
			"Work Mode":     app.WorkMode,
		})
		th.Conclude()
	}
}
