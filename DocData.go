package godocgen

import (
	"bytes"
	"go/doc"
	"go/printer"
	"go/token"
	"path/filepath"
	"strings"

	"gitlab.com/zoralab/godocgen/internal/generator"
)

const (
	docIndicator  = "// "
	newLine       = "\n"
	indentTab     = "\t"
	declOpener    = "("
	declCloser    = ")"
	funcIndicator = "func "

	cfgPrinterMode     = printer.TabIndent | printer.UseSpaces
	cfgPrinterTabWidth = 8
)

func processDecl(fileSet *token.FileSet,
	decl interface{}) string {
	var h bytes.Buffer

	cfg := &printer.Config{
		Mode:     cfgPrinterMode,
		Tabwidth: cfgPrinterTabWidth,
	}
	_ = cfg.Fprint(&h, fileSet, decl)

	return h.String()
}

// DataElement is the data structure holding specific data values for `Data`.
//
// The structure contains elements that require initialization. Hence, it is
// not safe to create using `struct{}` method.
//
// Depending on its associated relationship with its `Data` structure,
// DataElement can be represented in many ways, including towards itself.
//
// ----
//
// For `Examples` association from `Data`:
//   1. `DataElement`'s `Examples` holds all the `Data`'s examples in its
//      `List`, which is called **Examples Group**.
//   2. For each elements in **Examples Group**'s `List`, it is called
//      **Example Elements**.
//
//   EXAMPLES GROUP
//   * `Header`   - not applicable: it is empty
//   * `Receiver` - not applicable: it is empty
//   * `Synopsis` - the synopsis of the examples
//   * `Value`    - not applicable: it is empty
//   * `Codes`    - not applicable: either `nil` or empty
//   * `Examples` - list of **Example Elements**.
//   * `List`     - not applicable: either `nil` or empty
//
//   EXAMPLES ELEMENTS
//   * `Header`   - suffix of the example (`_second`, `_third`, etc.)
//   * `Receiver` - name of the example with trimmed suffix
//   * `Synopsis` - the descriptions of the example
//   * `Value`    - output value of the example
//   * `Codes`    - the lines of codes describing the example (trimmed newline
//                  and tab characters)
//   * `Examples` - not applicable: either `nil` or empty
//   * `List`     - not applicable: either `nil` or empty
//
// ----
//
// For `Constants` and `Variables` association from `Data`:
//   1. `DataElement`'s `List` holds all the `Data`'s constants/variables group
//      in a list of `DataElements` called **Constants/Variables Group**.
//   2. In each "constant/variable group", its `List` has all the
//      constants/variables called **Constant/Variable Elements**.
//
//   CONSTANT/VARIABLES GROUP
//   * `Header`   - group type in code form. Either `const` or `var`
//   * `Receiver` - not applicable: it is empty
//   * `Synopsis` - the description of the example group
//   * `Value`    - not applicable: it is empty
//   * `Codes`    - not applicable: either `nil` or empty
//   * `Examples` - not applicable: either `nil` or empty
//   * `List`     - list of **Constant/variable elements**
//
//   CONSTANT/VARIABLE ELEMENTS
//   * `Header`   - suffix of the example (`_second`, `_third`, etc.)
//   * `Receiver` - name of the example with trimmed suffix
//   * `Synopsis` - the descriptions of the example
//   * `Value`    - output value of the example
//   * `Codes`    - the lines of codes describing the example (trimmed newline
//                  and tab characters)
//   * `Examples` - not applicable: either `nil` or empty
//   * `List`     - not applicable: either `nil` or empty
//
// ----
//
// For `Functions` and `Methods` association from `Data`:
//   1. The `Functions`/`Methods` holds all its functions/methods in its `List`
//      called **Functions/Methods Group**.
//   2. For each elements in **Functions/Methods Group**, it holds the data for
//      the function/method called **Function/Method Elements**.
//
//   FUNCTIONS/METHODS GROUP
//   * `Header`   - not applicable: it is empty
//   * `Receiver` - not applicable: it is empty
//   * `Synopsis` - the description of the functions/methods group
//   * `Value`    - not applicable: it is empty
//   * `Codes`    - not applicable: either `nil` or empty
//   * `Examples` - not applicable: either `nil` or empty
//   * `List`     - list of **Function Elements**
//
//   FUNCTION/METHOD ELEMENTS
//   * `Header`   - synopsis of the functions
//   * `Receiver` - Only applicable for `method` which is function receiver type
//   * `Synopsis` - not applicable: it is empty
//   * `Value`    - function name with `Receiver` if exists, without `func`
//                  keyword)
//   * `Codes`    - not applicable: either `nil` or empty
//   * `Examples` - list of associated examples
//   * `List`     - not applicable: either `nil` or empty
type DataElement struct {
	Header   string
	Synopsis string
	Receiver string
	Value    string
	Codes    []string
	Examples []*DataElement
	List     []*DataElement
}

func newDataElement() *DataElement {
	return &DataElement{
		Codes:    []string{},
		Examples: []*DataElement{},
		List:     []*DataElement{},
	}
}

func (dc *DataElement) processConstOrVar(dl []*doc.Value,
	fileSet *token.FileSet) {
	for _, d := range dl {
		// create new data list
		gp := newDataElement()

		// process synopsis
		gp.Synopsis = strings.TrimSpace(d.Doc)

		// process overall output
		s := processDecl(fileSet, d.Decl)
		sl := strings.Split(s, newLine+indentTab)

		// populate by parsing each lines
		e := newDataElement()
		header := []string{}

		for i, v := range sl {
			switch {
			case i == 0:
				gp.Header = strings.TrimSuffix(v, declOpener)
				gp.Header = strings.TrimSpace(gp.Header)

				continue
			case strings.HasPrefix(v, docIndicator):
				header = append(header,
					strings.TrimPrefix(v, docIndicator),
				)

				continue
			case i == len(sl)-1:
				v = strings.TrimSuffix(v, declCloser)
				fallthrough
			default:
				e.Header = strings.Join(header, " ")
				e.Header = strings.TrimSpace(e.Header)
				e.Value = strings.TrimSuffix(v, newLine)
				gp.List = append(gp.List, e)
				e = newDataElement()
				header = []string{}

				continue
			}
		}

		// merge with main data group
		dc.List = append(dc.List, gp)
	}
}

func (dc *DataElement) processExamples(examples []*doc.Example,
	fileSet *token.FileSet) {
	// nothing to process, bailing
	if examples == nil {
		return
	}

	var buf bytes.Buffer
	var s string
	var de *DataElement

	for _, e := range examples {
		// create new data element
		de = newDataElement()

		// process example suffix
		de.Header = strings.Title(e.Suffix)

		// process example name
		de.Receiver = strings.TrimRight(e.Name, e.Suffix)
		de.Receiver = strings.TrimRight(de.Receiver, "_")
		de.Receiver = strings.ReplaceAll(de.Receiver, "_", ".")

		// process doc
		de.Synopsis = strings.TrimSpace(e.Doc)

		// process example output
		de.Value = e.Output

		// process code blocks
		buf.Reset()
		printer.Fprint(&buf, fileSet, e.Code)
		s = buf.String()
		s = s[1 : len(s)-1] // trim braces
		s = strings.ReplaceAll(s, "\t", "")
		s = strings.TrimSpace(s)
		de.Codes = strings.Split(s, "\n")

		if len(de.Codes) == 0 ||
			(len(de.Codes) == 1 && de.Codes[0] == "") {
			continue
		}

		// merge with main data group
		dc.Examples = append(dc.Examples, de)
	}
}

func (dc *DataElement) processFunction(dl []*doc.Func, fileSet *token.FileSet) {
	for _, d := range dl {
		// create new element
		de := newDataElement()

		// process function documentations as header
		de.Header = strings.TrimSpace(d.Doc)

		// process function title as value
		de.Value = processDecl(fileSet, d.Decl)
		de.Value = strings.TrimPrefix(de.Value, funcIndicator)

		// process receiver
		if d.Recv != "" {
			de.Receiver = d.Recv
		}

		// process example
		de.processExamples(d.Examples, fileSet)

		// merge with main data group
		dc.List = append(dc.List, de)
	}
}

// Data is the data structure holding `Package` or `Type` level.
//
// This structure contains elements that needs initialization. Hence, to be
// on the safe side, you should use `NewData()` function if you ever need to
// create one.
//
// For `Package` subject, the elements' representations are as follow:
// 1. `Header`       - name of the package
// 2. `ImportPath`   - the import path statement
// 3. `Synopsis`     - the descriptions of the package
// 4. `RelativePath` - the relative path of the package to its root directory
// 5. `Filepath`     - the output filepath (for file rendering)
// 6. `Codes`        - not applicable: either `nil` or empty
// 7. `Examples`     - the element holding the `List` of examples
// 8. `Constants`    - the element holding the `List` of constants
// 9. `Variables`    - the element holding the `List` of variables
// 10. `Functions`   - the element holding the `List` of functions
// 11. `Methods`     - not applicable: either `nil` or empty
// 12. `Types`       - array of `Data` holding `Type` details
//
// For `Type` subject, the elements' representations are as follow:
// 1. `Header`       - name of the `Type`
// 2. `ImportPath`   - not applicable: it is always empty
// 3. `Synopsis`     - the descriptions of the `Type`
// 4. `RelativePath` - not applicable: it is always empty
// 5. `Filepath`     - not applicable: it is always empty
// 6. `Codes`        - Codes in multiple line
// 7. `Examples`     - the element holding the `List` of examples
// 8. `Constants`    - the element holding the `List` of constants
// 9. `Variables`    - the element holding the `List` of variables
// 10. `Functions`   - the element holding the `List` of functions
// 11. `Methods`     - the element holding the `List` of functions with receiver
// 12. `Types`       - not applicable: either `nil` or empty
type Data struct {
	Header       string
	ImportPath   string
	Synopsis     string
	RelativePath string
	Filepath     string
	Codes        []string
	Examples     *DataElement
	Constants    *DataElement
	Variables    *DataElement
	Functions    *DataElement
	Methods      *DataElement
	Types        []*Data
}

// NewData creates a new Data object and return its pointer as output.
//
// This is the recommended function to create new *Data structure type over the
// conventional `&Data{...}` method.
//
// Unless you're using App as LibraryMode for development or testing, you do
// not need to use this function since `App` will generate the `Data` objects
// automatically. This function was meant to facilitate Developers to create
// mocking data for unit testing.
func NewData() *Data {
	return &Data{
		Codes:     []string{},
		Examples:  newDataElement(),
		Constants: newDataElement(),
		Variables: newDataElement(),
		Functions: newDataElement(),
		Methods:   newDataElement(),
		Types:     []*Data{},
	}
}

func (dc *Data) processTypes(types []*doc.Type, fileSet *token.FileSet) {
	for _, d := range types {
		t := NewData()

		// filling name
		t.Header = d.Name

		// filling synopsis
		t.Synopsis = strings.TrimSpace(d.Doc)

		// process type code body
		s := processDecl(fileSet, d.Decl)
		t.Codes = strings.Split(s, newLine)

		// process type related constructs
		t.Constants.processConstOrVar(d.Consts, fileSet)
		t.Variables.processConstOrVar(d.Vars, fileSet)
		t.Functions.processFunction(d.Funcs, fileSet)
		t.Methods.processFunction(d.Methods, fileSet)
		t.Examples.processExamples(d.Examples, fileSet)

		// merge with main data list
		dc.Types = append(dc.Types, t)
	}
}

func (dc *Data) parse(targetPath string,
	workPath string,
	outputPath string,
	filename string,
	testConfig map[int]bool) error {
	raw := &generator.DocData{}
	if !raw.Configure(targetPath) {
		return createError(ErrorConfigureDocData, targetPath)
	}

	raw.Generate()

	if raw.Err == nil && testConfig[testSimulateGeneratorError] {
		raw.Err = createError(errorSimulateGeneratorError, targetPath)
	}

	if raw.Err != nil {
		return raw.Err
	}

	dc.Header = raw.Name
	dc.Synopsis = raw.Package.Doc
	dc.ImportPath = raw.Package.ImportPath
	dc.Examples.processExamples(raw.Package.Examples, raw.FileSet)
	dc.Constants.processConstOrVar(raw.Package.Consts, raw.FileSet)
	dc.Variables.processConstOrVar(raw.Package.Vars, raw.FileSet)
	dc.Functions.processFunction(raw.Package.Funcs, raw.FileSet)
	dc.processTypes(raw.Package.Types, raw.FileSet)

	// configure output filepath
	switch {
	case outputPath == Terminal:
		if testConfig[testMode] {
			dc.RelativePath = strings.TrimPrefix(raw.Path, workPath)
			dc.Filepath, _ = filepath.Abs(testTerminalOutputPath)
		}
	default:
		dc.RelativePath = strings.TrimPrefix(raw.Path, workPath)
		dc.Filepath = filepath.Join(outputPath, dc.RelativePath)
		dc.Filepath = filepath.Join(dc.Filepath, filename)
	}

	dc.RelativePath = strings.TrimPrefix(dc.RelativePath, "/")

	return nil
}
